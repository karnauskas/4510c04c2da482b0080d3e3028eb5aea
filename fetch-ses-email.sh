#!/bin/bash

# Fetch emails from Amazon S3 (deposited by the Amazon SES receiver's S3 action)
# and feed to procmail. In the spirit of fetchmail, but using S3 instead of SMTP.

BUCKET=my-bucket-name
export AWS_PROFILE=my-aws-profile

PROCMAIL="/usr/bin/procmail"

# for each object in the bucket...
for F in `aws s3 ls "s3://${BUCKET}/" | awk '{print $4;}'`; do
    if [ "${F}" == "AMAZON_SES_SETUP_NOTIFICATION" ]; then
        continue # ignore this object SES creates during set-up
    fi

    # download the object and feed it on stdin to procmail
    if ! aws s3 cp "s3://${BUCKET}/${F}" - | ${PROCMAIL}; then
        echo "S3->Procmail fetch of ${F} failed with status $?"
    else
        echo "$0 successfully fetched ${F}" >> "${HOME}/.fetchlog"

        # success - delete the S3 object
        aws s3 rm "s3://${BUCKET}/${F}" --quiet
    fi
done